from typing import List, Dict
from utils import dec_validate_int_param


class BuildStats:
    """
    this class will sort the list data and will compute the information
    """

    def __init__(self):
        self._data: List[int] = []
        self._statistics: Dict = {}

    def _sort_data(self, data: List[int]) -> List[int]:
        """
        Allow to sort the elements, here I'm using Counting Sort
        Counting Sort: A sorting algorithm that works by counting
        the frequency of each
        element in the list and using that information to sort the list.
        """
        result = []
        for i in range(max(data) + 1):
            result.extend([i] * data.count(i))
        return result

    def build_stats(self, data: List[int] = []) -> object:
        """
        This methods compute the statistics and allow less, between and greater do the operations faster
        """
        data = data or self._data
        if not data:
            raise ValueError("Is require to add some information to compute")
        self._data = self._sort_data(data)

        statistics = {}
        for element in self._data:
            statistics[element] = {
                "_min": len(self._data[: self._data.index(element)]),
                "_max": len(self._data[self._data.index(element) + 1 :]),
                "_pos": self._data.index(element),
            }
        self._statistics = statistics

        return self

    def clear(self):
        """
        This methods deletes all the information in the data and statistics
        """
        self._data = []
        self._statistics = {}


class DataCapture(BuildStats):
    """
    Allow to compute some statistics base on the inputs
    """

    @dec_validate_int_param
    def add(self, number: int) -> list[int]:
        """
        Allow to add a new number in the collection
        """
        self._data.append(number)
        return self._data

    @dec_validate_int_param
    def less(self, number: int, data: List[int] = []) -> int:
        """
        Return the values less than the parameter "number"
        """
        self.build_stats(data)
        if number not in self._statistics:
            return False
        return self._statistics[number].get("_min")

    @dec_validate_int_param
    def between(self, number_from: int, number_to: int, data: List[int] = []) -> int:
        """
        Return the values between the parameters "number_from" and "number_to"
        """
        self.build_stats(data)

        if number_from >= number_to:
            raise ValueError(
                "The second parameter always should be greater than the first one"
            )

        if number_from not in self._statistics or number_to not in self._statistics:
            return False
        pos_number_form = self._statistics[number_from].get("_pos")
        pos_number_to = self._statistics[number_to].get("_pos")
        return abs(pos_number_form - pos_number_to) + 1

    @dec_validate_int_param
    def greater(self, number: int, data: List[int] = []) -> int:
        """
        Return the values greater than the parameter "number"
        """
        self.build_stats(data)
        if number not in self._statistics:
            return False
        return self._statistics[number].get("_max")
