def dec_validate_int_param(func):
    """
    Allow to validate if the parameters are positive integers
    """

    def wrapper(self, *args, **kwargs):
        for param in args:
            if not isinstance(param, list):
                if not isinstance(param, int) or param < 0:
                    raise ValueError(f"The parameter {param} should be a possitive integer")

        result = func(self, *args, **kwargs)
        return result

    return wrapper
