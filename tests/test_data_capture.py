from sys import path

path.append(".")
import pytest
from typing import List
from DataCapture import DataCapture
from fixtures import data1, data2


class TestDataCapture:
    """
    Check if the available methods in DataCapture class are working properly
    """

    def _add_data(self, data_capture: DataCapture, data: List[int]):
        """
        Allow to fill out the object
        """
        for number in data:
            data_capture.add(number)

    def test_add_method(self):
        """
        Allow to check if the add method is working fine
        """
        data_capture = DataCapture()
        data_capture.add(1)
        data_capture.add(20)
        data = data_capture.add(100)

        assert [1, 20, 100] == data, "The result is not correct"

    def test_less_method(self, data1):
        """
        Allow to check if the less method really returns the quantity of numbers that are less than the parameter
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data1)

        stats = data_capture.build_stats()
        assert stats.less(4) == 2, "The quantity of numbers less than 4 are 2 numbers"

    def test_greater_method(self, data1):
        """
        Allow to check if the greater method really returns the quantity of numbers that are greater than the parameter
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data1)

        stats = data_capture.build_stats()
        assert (
            stats.greater(4) == 2
        ), "The quantity of numbers greater than 4 are 2 numbers"

    def test_1_between_method(self, data1):
        """
        Allow to check if the between method really returns the numbers that are between the two parameters
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data1)

        stats = data_capture.build_stats()
        assert stats.between(3, 6) == 4, "The quantity of numbers between 3 and 6 is 4"

    def test_2_between_method(self, data2):
        """
        Allow to check if the between method really returns the numbers that are between the two parameters
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data2)

        stats = data_capture.build_stats()
        assert stats.between(4, 9) == 3, "The quantity of numbers between 4 and 9 is 3"

    def test_methods_decorator_less(self, data1):
        """
        The next test will check if calling the methods directly works fine without to call build_stats.
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data1)

        assert (
            data_capture.less(4) == 2
        ), "The quantity of numbers less than 4 are 2 numbers"
        assert (
            data_capture.greater(4) == 2
        ), "The quantity of numbers greater than 4 are 2 numbers"
        assert (
            data_capture.between(3, 6) == 4
        ), "The quantity of numbers between 3 and 6 is 4"

    def test_methods_decorator_greater(self, data1):
        """
        The next test will check if calling the methods directly works fine without to call build_stats.
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data1)

        assert (
            data_capture.greater(4) == 2
        ), "The quantity of numbers greater than 4 are 2 numbers"

    def test_methods_decorator_between(self, data1):
        """
        The next test will check if calling the methods directly works fine without to call build_stats.
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data1)

        assert (
            data_capture.between(3, 6) == 4
        ), "The quantity of numbers between 3 and 6 is 4"

    def test_validations_possitive_integers(self, data1):
        """
        Check if the validations of the parameters works good
        """
        data_capture = DataCapture()

        with pytest.raises(ValueError):
            data_capture.add(-1)

        with pytest.raises(ValueError):
            data_capture.clear()
            for number in [1, 2, 3, -1]:
                data_capture.add(number)

        for number in data1:
            data_capture.add(number)

        with pytest.raises(ValueError):
            data_capture.less(-1)

        with pytest.raises(ValueError):
            data_capture.greater("1")

        with pytest.raises(ValueError):
            data_capture.between(1, -4)

        # Checking if the code will raise any problem if we introduce values that are no in the list
        data_capture.clear()
        assert (
            data_capture.less(1, data1) is False
        ), "The code can not find the parameter"
        assert (
            data_capture.greater(10, data1) is False
        ), "The code can not find the parameter"
        assert (
            data_capture.between(12, 16, data1) is False
        ), "The code can not find the parameter"

    def test_validations_between(self, data1):
        """
        Check if the second parameters is greater than the first one
        """
        data_capture = DataCapture()
        self._add_data(data_capture, data1)

        with pytest.raises(ValueError):
            data_capture.between(6, 3)

        with pytest.raises(ValueError):
            data_capture.between(6, 5)

    def test_methods_without_add_less(self, data1):
        """
        Allow to check if the user can use the methods without to use the add method
        """
        data_capture = DataCapture()
        assert (
            data_capture.less(4, data1) == 2
        ), "The quantity of numbers less than 4 are 2 numbers"

    def test_methods_without_add_greater(self, data1):
        """
        Allow to check if the user can use the methods without to use the add method
        """
        data_capture = DataCapture()
        assert (
            data_capture.greater(4, data1) == 2
        ), "The quantity of numbers greater than 4 are 2 numbers"

    def test_methods_without_add_between(self, data1):
        """
        Allow to check if the user can use the methods without to use the add method
        """
        data_capture = DataCapture()
        assert (
            data_capture.between(4, 9, data1) == 3
        ), "The quantity of numbers between 4 and 9 is 3"
