import pytest


@pytest.fixture
def data1():
    """
    Sample data
    """
    return [3, 9, 3, 4, 6]


@pytest.fixture
def data2():
    """
    Sample data
    """
    return [3, 9, 3, 4, 6, 10, 12]
