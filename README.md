# Technical Challenge Senior Python Engineer

The objective of the challenge is to create a program that a allow the user do the next statistics:

add: Allow to add elements for the operations.

less: Allow to know the quantity of numbers less than the parameter specific by the user.

between: Allow to know the quantity of numbers between the range specific by the user.

greater: Allow to know the quantity of numbers greater than the parameters specific by the user.

The program also will have a method with name build_stats that will do the require calculations.

The operations add, less, between, greater will have a constant time O(1).
The operation build_stats will have a constant time O(n).

The program will have all the best practices that I know and I will include some test to check that the functionality is working fine.

Let's start with the require things to run the program.

## Getting started
Note: this challenge is created using python 3.10.6

## Installation
1. First you need to create the virtual environment with the next command: python3 -m venv .venv
2. Use the next command to enable the virtual environment: source .venv/bin/activate
3. Please install all the require dependencies: pip3 install -r requirements.txt

## Run the program
If you need to see how the program works, please use the next command: python3 main.py

## Run the test cases
If you need to run the test cases, please use the next command: pytest -q tests/test_data_capture.py
