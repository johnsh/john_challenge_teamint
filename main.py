from DataCapture import DataCapture

if __name__ == '__main__':
    data_capture = DataCapture()
    data_capture.add(3)
    data_capture.add(9)
    data_capture.add(3)
    data_capture.add(4)
    data_capture.add(6)
    stats = data_capture.build_stats()

    print('Creating a list of 5 elements')
    print( [3,9,3,4,6] )

    print('Result of the method less(4)', stats.less(4))
    print('Result of the method greater(4)', stats.greater(4))
    print('Result of the method between(3,6)', stats.between(3,6))
